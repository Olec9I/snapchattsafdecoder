// <auto-generated />
namespace SnapchatTSAFDecoder.Output
{
    using System.IO;
    using Newtonsoft.Json;
    using Types;

    /// <summary>
    /// This class implements writing all extracted information to output archive
    /// </summary>
    public static class OutputPrinter
    {
        /// <summary>
        /// Writes Snapchat data to JSON
        /// </summary>
        /// <param name="filename">File to write data</param>
        /// <param name="data">Parsed conversations from Snapchat</param>
        public static void PrintSnapchatDataToJson(string filename, SnapchatData data)
        {
            var serializedData = JsonConvert.SerializeObject(data);
            File.WriteAllText(filename, serializedData);
        }
    }
}