// <auto-generated />
namespace SnapchatTSAFDecoder.Parsing.Types 
{
    using System;
    using System.IO;

    /// <summary>
    /// This class implements TSAF unsigned int16
    /// </summary>
    public class UInt16Tsaf : BasicTypeTsaf
    {
        public ushort Value { get; private set; }
        protected override int ParseType(BinaryReader reader, int byteCounter)
        {
            byteCounter = ParserUtils.SkipPadding(reader, byteCounter, 2);
            var valueBytes = reader.ReadBytes(2);
            byteCounter += 2;
            Value = BitConverter.ToUInt16(valueBytes);

            return byteCounter;
        }

        public override string GetTypeName() => "unsignedInt16";
    }
}