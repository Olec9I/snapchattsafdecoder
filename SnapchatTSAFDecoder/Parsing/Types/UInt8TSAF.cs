// <auto-generated />
namespace SnapchatTSAFDecoder.Parsing.Types
{ 
    using System.IO;
    
    /// <summary>
    /// This class implements TSAF unsigned int8
    /// </summary>
    public class UInt8Tsaf : BasicTypeTsaf
    { 
        public byte Value { get; private set; }
        
        protected override int ParseType(BinaryReader reader, int byteCounter)
        {
            Value = reader.ReadByte();
            byteCounter++;

            return byteCounter;
        }

        public void FillManually(byte value)
        {
            IsFilled = true;
            Value = value;
        }

        public override string GetTypeName() => "unsignedInt8";
    }
}