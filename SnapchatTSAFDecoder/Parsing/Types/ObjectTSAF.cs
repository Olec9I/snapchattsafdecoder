// <auto-generated />
namespace SnapchatTSAFDecoder.Parsing.Types
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    
    /// <summary>
    /// This class implements TSAF object
    /// </summary>
    public class ObjectTsaf : BasicTypeTsaf
    {
        public DefinedObject Value { get; private set; }
        
        protected override int ParseType(BinaryReader reader, int byteCounter)
        {
            var context = ParserContext.Instance;
            var classNumberByte = reader.ReadByte();
            byteCounter++;
            int classNumber = classNumberByte;

            DefinedClass implementedClass = context.DefinedClasses[classNumber];
            Console.WriteLine("This object implements " + implementedClass.Name);

            var fields = new List<BasicTypeTsaf>();
            
            for (var fieldIndex = 0; fieldIndex < implementedClass.Fields.Count; fieldIndex++)
            {
                ClassFieldType field = implementedClass.Fields[fieldIndex];
                BasicTypeTsaf fieldBasicType;
                switch (field)
                {
                    case ClassFieldType.Tba:
                        var parseResult = ParserUtils.DefineType(reader, byteCounter);
                        byteCounter = parseResult.ByteCounter;
                        fieldBasicType = parseResult.ParsedField;
                        break;
                    case ClassFieldType.Bool:
                        fieldBasicType = new BoolTsaf();
                        break;
                    case ClassFieldType.Int32:
                        fieldBasicType = new Int32Tsaf();
                        break;
                    case ClassFieldType.Int64:
                        fieldBasicType = new Int64Tsaf();
                        break;
                    default:
                        throw new Exception("Not implemented type from class");
                }

                byteCounter = fieldBasicType.Parse(reader, byteCounter);
                ParserUtils.TrackParsed(fieldBasicType);
                if (!ParserUtils.IsClazz(fieldBasicType))
                {
                    fields.Add(fieldBasicType);
                }
                else
                {
                    fieldIndex--;
                }
            }
            
            Value = new DefinedObject(implementedClass, fields);
            
            return byteCounter;
        }

        public override string GetTypeName() => "object";
    }
}