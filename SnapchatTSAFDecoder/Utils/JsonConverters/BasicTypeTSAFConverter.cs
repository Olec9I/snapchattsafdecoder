// <auto-generated />
namespace SnapchatTSAFDecoder.Utils.JsonConverters
{
    using System;
    using System.Globalization;
    using Newtonsoft.Json;
    using Parsing.Types;
    
    /// <summary>
    /// This class implements conversion of all TSAF types to JSON
    /// </summary>
    public class BasicTypeTsafConverter : JsonConverter<BasicTypeTsaf>
    {
        public override void WriteJson(JsonWriter writer, BasicTypeTsaf value, JsonSerializer serializer)
        {
            switch (value.GetTypeName())
            {
                case "array":
                    var arrayValue = (ArrayTsaf)value;
                    writer.WriteStartArray();

                    foreach (var elem in arrayValue.Value)
                    {
                        if (elem is ArrayTsaf || elem is DictionaryTsaf || elem is ObjectTsaf || elem is NamedDictChangingLengthTsaf)
                        {
                            writer.WriteRawValue(JsonConvert.SerializeObject(elem, new BasicTypeTsafConverter()));
                        }
                        else
                        {
                            writer.WriteValue(JsonConvert.SerializeObject(elem, new BasicTypeTsafConverter()));
                        } 
                    }
                
                    writer.WriteEndArray();
                    break;
                case "dict":
                    var dictValue = (DictionaryTsaf)value;
                    writer.WriteStartObject();

                    foreach (var elem in dictValue.Value)
                    {
                        writer.WritePropertyName(JsonConvert.SerializeObject(elem.Key, new BasicTypeTsafConverter()));

                        if (elem.Value is ArrayTsaf || elem.Value is DictionaryTsaf || elem.Value is ObjectTsaf || elem.Value is NamedDictChangingLengthTsaf)
                        {
                            writer.WriteRawValue(JsonConvert.SerializeObject(elem.Value, new BasicTypeTsafConverter()));
                        }
                        else 
                        {
                            writer.WriteValue(JsonConvert.SerializeObject(elem.Value, new BasicTypeTsafConverter())); 
                        } 
                    }
                
                    writer.WriteEndObject();
                    break;
                case "object":
                    var objectValue = (ObjectTsaf)value;
                    writer.WriteStartObject();
                    writer.WritePropertyName($"objOf {objectValue.Value.Clazz.Name}");
                    writer.WriteRawValue(JsonConvert.SerializeObject(objectValue.Value.Fields, new ListConverter()));
                    writer.WriteEndObject();
                    break;
                case "stringPointer8":
                    var strPtr8 = (StringPointer8Tsaf)value;
                    writer.WriteRawValue(strPtr8.PointedString);
                    break;
                case "stringPointer16":
                    var strPtr16 = (StringPointer16Tsaf)value;
                    writer.WriteRawValue(strPtr16.PointedString);
                    break;
                case "string":
                    var str = (StringTsaf)value;
                    writer.WriteRawValue(str.StringValue);
                    break;
                case "bool":
                    var boolValue = (BoolTsaf)value;
                    writer.WriteRawValue(boolValue.Value.ToString());
                    break;
                case "strangeConst":
                    writer.WriteRawValue("0x2E");
                    break;
                case "double":
                    var doubleValue = (DoubleTsaf)value;
                    writer.WriteRawValue(doubleValue.Value.ToString(CultureInfo.InvariantCulture));
                    break;
                case "int16":
                    var int16Value = (Int16Tsaf)value;
                    writer.WriteRawValue(int16Value.Value.ToString());
                    break;
                case "int32": 
                    var int32Value = (Int32Tsaf) value;
                    writer.WriteRawValue(int32Value.Value.ToString());
                    break;
                case "int64":
                    var int64Value = (Int64Tsaf)value;
                    writer.WriteRawValue(int64Value.Value.ToString());
                    break;
                case "int8":
                    var int8Value = (Int8Tsaf)value;
                    writer.WriteRawValue(int8Value.Value.ToString());
                    break;
                case "null":
                    writer.WriteRawValue("NULL");
                    break;
                case "objectPointer16":
                    var objPtr16 = (ObjectPointer16Tsaf)value;
                    writer.WriteRawValue("objPtr" + objPtr16.TypeValue.Value);
                    break;
                case "objectPointer8":
                    var objPtr8 = (ObjectPointer8Tsaf)value;
                    writer.WriteRawValue("objPtr" + objPtr8.TypeValue.Value);
                    break;
                case "single":
                    var singleValue = (SingleTsaf)value;
                    writer.WriteRawValue(singleValue.Value.ToString(CultureInfo.InvariantCulture));
                    break;
                case "unsignedInt16":
                    var uint16Value = (UInt16Tsaf)value;
                    writer.WriteRawValue(uint16Value.Value.ToString());
                    break;
                case "unsignedInt8":
                    var uint8Value = (UInt8Tsaf)value;
                    writer.WriteRawValue(uint8Value.Value.ToString());
                    break;
                case "systemBlob":
                    var blobValue = (SystemBlobTsaf)value;
                    writer.WriteRawValue("Blob: " + blobValue);
                    break;
                case "namedDictionaryChangingLength":
                    var namedDictValue = (NamedDictChangingLengthTsaf)value;
                    writer.WriteStartObject();

                    foreach (var elem in namedDictValue.DefinedNamedDictValue.Dictionary)
                    {
                        writer.WritePropertyName(JsonConvert.SerializeObject(elem.Key, new BasicTypeTsafConverter()));

                        if (elem.Value is ArrayTsaf || elem.Value is DictionaryTsaf || elem.Value is ObjectTsaf || elem.Value is NamedDictChangingLengthTsaf)
                        {
                            writer.WriteRawValue(JsonConvert.SerializeObject(elem.Value, new BasicTypeTsafConverter()));
                        }
                        else
                        {
                            writer.WriteValue(JsonConvert.SerializeObject(elem.Value, new BasicTypeTsafConverter())); 
                        } 
                    }
                
                    writer.WriteEndObject(); 
                    break; 
                default:
                    writer.WriteRawValue("^");
                    break;
            }
        }

        public override BasicTypeTsaf ReadJson(
            JsonReader reader,
            Type objectType,
            BasicTypeTsaf existingValue,
            bool hasExistingValue,
            JsonSerializer serializer) 
            => throw new NotImplementedException();
    }
}