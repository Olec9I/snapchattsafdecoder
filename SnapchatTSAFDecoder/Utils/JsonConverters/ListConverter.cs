// <auto-generated />
namespace SnapchatTSAFDecoder.Utils.JsonConverters
{
    using System;
    using System.Collections.Generic;
    using Newtonsoft.Json;
    using Parsing.Types;
    
    /// <summary>
    /// This class converts List of TSAF types to JSON format
    /// </summary>
    public class ListConverter : JsonConverter<List<BasicTypeTsaf>>
    {
        public override void WriteJson(JsonWriter writer, List<BasicTypeTsaf> value, JsonSerializer serializer)
        {
            writer.WriteStartArray();

            foreach (var elem in value)
            {
                if (elem is ArrayTsaf || elem is DictionaryTsaf || elem is ObjectTsaf || elem is NamedDictChangingLengthTsaf)
                {
                    writer.WriteRawValue(JsonConvert.SerializeObject(elem, new BasicTypeTsafConverter()));
                }
                else
                {
                    writer.WriteValue(JsonConvert.SerializeObject(elem, new BasicTypeTsafConverter()));
                } 
            }
                
            writer.WriteEndArray();
        }
        
        public override List<BasicTypeTsaf> ReadJson(
            JsonReader reader, 
            Type objectType,
            List<BasicTypeTsaf> existingValue, 
            bool hasExistingValue, 
            JsonSerializer serializer
            ) => 
            throw new NotImplementedException();
    }
}