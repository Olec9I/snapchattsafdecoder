# SnapchatDecoder

This tool can extract data from file chatConversationStore.plist of Snapchat iOS app.

The extracted useful data is used to generate JSON file with the following structure:

```json
{
  "username":"Name",
  "conversations":{
    "Recipient's name~Sender's name":[
      {
        "ReceiveTime":"2020-05-23T13:20:36Z",
        "MessageText":"Hi",
        "Location":{
            "Latitude":59.8815673,
            "Longitude":30.2750078
        },
        "Sender":"Name",
        "Sticker":{
	    "StickerType":"CLASSIC STICKER",
	    "StickerFilename":"sticker_2.png",
	    "StickerTags":[
	    	"DANCE",
		"DANCING",
		"LETS DANCE"
	    ]
	},
        "MediaFile":[
	    "media_1.jpg"
	]
      }
    ]
  }
}
```

What is done:
* parsing of file to TSAF structures
* generating JSON file, containing all TSAF structures
* generating JSON file with useful data: 
	* text messages
	* sender's and recipient's name
	* time of receiving message and sending message
	* GPS location, if it is sent
* extraction of media files
* extraction of stickers, emojis, bitmojis

Result of analyzing is packed to zip archive which contains json with dialogs, all extracted media files and stickers.

---
**How to build:**

First, you need .NET Core SDK 3.1.302. 

Installation instructions [here](https://dotnet.microsoft.com/download).

Installation instructions for Windows can be found [here](https://dotnet.microsoft.com/download/dotnet-core/thank-you/sdk-3.1.302-windows-x64-installer).

**How to run:**
```bash
cd SnapchatTSAFDecoder\SnapchatTSAFDecoder
dotnet run <path-to-chatConversationStore.plist or path-to-Snapchat-app-directory>
```